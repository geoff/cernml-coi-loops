<!--
SPDX-FileCopyrightText: 2020-2023 CERN
SPDX-FileCopyrightText: 2023 GSI Helmholtzzentrum für Schwerionenforschung
SPDX-FileNotice: All rights not expressly granted are reserved.

SPDX-License-Identifier: GPL-3.0-or-later OR EUPL-1.2+
-->

# Changelog

## Unreleased

- OTHER: Open-source this package by adding the appropriate license notices.

## v0.2.0

- Update requiremed `cernml-coi` to v0.8.9.
- Add support for Python 3.9.
- Add support for `cernml.coi.FunctionOptimizable.override_skeleton_points()`.

## v0.1.0

- Initial release.
