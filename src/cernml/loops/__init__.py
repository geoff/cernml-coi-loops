# SPDX-FileCopyrightText: 2020-2023 CERN
# SPDX-FileCopyrightText: 2023 GSI Helmholtzzentrum für Schwerionenforschung
# SPDX-FileNotice: All rights not expressly granted are reserved.
#
# SPDX-License-Identifier: GPL-3.0-or-later OR EUPL-1.2+

"""GUI-less optimization loops for the Common Optimization Interfaces."""

from ._buffers import LimitedStepBuffer, Step, StepBuffer
from ._builder import (
    CannotInstantiateProblem,
    CannotStartRun,
    DuplicateSpec,
    Metadata,
    MissingKwargs,
    NoProblemSelected,
    ProblemFactory,
    ProblemKwargsSpec,
    RunFactory,
    UnknownKwarg,
)
from ._callbacks import (
    Action,
    BaseResetBeginMessage,
    BaseResetEndMessage,
    Callback,
    CallbackList,
    ConstraintsEvalMessage,
    ExplicitAction,
    FunctionResetBeginMessage,
    FunctionResetEndMessage,
    IterationMessage,
    ObjectiveEvalMessage,
    OptBeginMessage,
    OptEndMessage,
    OptimizeStatus,
    RawCallback,
    RenderBeginMessage,
    RenderEndMessage,
    ResetBeginMessage,
    ResetEndMessage,
    RunBeginMessage,
    RunEndMessage,
    SimpleResetBeginMessage,
    SimpleResetEndMessage,
    SkeletonPointInfo,
)
from ._catching import catching_exceptions
from ._jobs import BadInitialPoint, Run, RunParams

__all__ = [
    "Action",
    "BadInitialPoint",
    "BaseResetBeginMessage",
    "BaseResetEndMessage",
    "Callback",
    "CallbackList",
    "CannotInstantiateProblem",
    "CannotStartRun",
    "ConstraintsEvalMessage",
    "DuplicateSpec",
    "ExplicitAction",
    "FunctionResetBeginMessage",
    "FunctionResetEndMessage",
    "IterationMessage",
    "LimitedStepBuffer",
    "Metadata",
    "MissingKwargs",
    "NoProblemSelected",
    "ObjectiveEvalMessage",
    "OptBeginMessage",
    "OptEndMessage",
    "OptimizeStatus",
    "ProblemFactory",
    "ProblemKwargsSpec",
    "RawCallback",
    "RenderBeginMessage",
    "RenderEndMessage",
    "ResetBeginMessage",
    "ResetEndMessage",
    "Run",
    "RunBeginMessage",
    "RunEndMessage",
    "RunFactory",
    "RunParams",
    "SimpleResetBeginMessage",
    "SimpleResetEndMessage",
    "SkeletonPointInfo",
    "Step",
    "StepBuffer",
    "UnknownKwarg",
    "catching_exceptions",
]
